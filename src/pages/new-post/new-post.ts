import { Component } from '@angular/core';
import { Camera, CameraOptions } from '@ionic-native/camera';
import { FileTransfer, FileTransferObject, FileUploadOptions } from '@ionic-native/file-transfer';
import { ActionSheetController, NavController, NavParams, App } from 'ionic-angular';
import { GlobalsProvider } from '../../providers/globals/globals';
import { RestProvider } from '../../providers/rest/rest';
import { HomePage } from '../home/home';
import { e } from '@angular/core/src/render3';
import { TabsPage } from '../tabs/tabs';
/**
 * Generated class for the NewPostPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@Component({
  selector: 'page-new-post',
  templateUrl: 'new-post.html',
})
export class NewPostPage {
  base64Image: any;
  imageFileName: any;
  base64Video: any;
  loader: any;
  selectedimage: any;
  Video: any;
  public photos: any = [];
  public old_photos: any = [];
  public old_photos_urls: any = [];
  
  public base64Image2: string;
  public fileImage: string;
  public responseData: any;
  public photo: any;

  postData = {
    post_id: '',
    post_title: '',
    post_description: '',
    post_user_id: '' + localStorage.getItem("user_id"),
    post_category: [],
    post_co_creators_id: '',
    insert: '',
    edit: '',
    delete: '',
    post_media: []
  };
  public co_creators = [
    {
      id: 1,
      img: 'https://source.unsplash.com/120x120',
      user_name: 'candelibas'
    },
    {
      id: 2,
      img: 'https://source.unsplash.com/120x120',
      user_name: 'maxlynch'
    },
    {
      id: 3,
      img: 'https://source.unsplash.com/120x120',
      user_name: 'ashleyosama'
    },
    {
      id: 4,
      img: 'https://source.unsplash.com/120x120',
      user_name: 'adam_bradley'
    },
    {
      id: 5,
      img: 'https://source.unsplash.com/120x120',
      user_name: 'linus_torvalds'
    },
    {
      id: 6,
      img: 'https://source.unsplash.com/120x120',
      user_name: 'linus_torvalds'
    },
    {
      id: 6,
      img: 'https://source.unsplash.com/120x120',
      user_name: 'linus_torvalds'
    },
    {
      id: 7,
      img: 'https://source.unsplash.com/120x120',
      user_name: 'linus_torvalds'
    },
    {
      id: 8,
      img: 'https://source.unsplash.com/120x120',
      user_name: 'linus_torvalds'
    },
    {
      id: 9,
      img: 'https://source.unsplash.com/120x120',
      user_name: 'linus_torvalds'
    },
    {
      id: 10,
      img: 'https://source.unsplash.com/120x120',
      user_name: 'linus_torvalds'
    }

  ];
  public categories = [];
  response: any;
  public post_media = [];
  constructor(public app: App, public navCtrl: NavController, public navParams: NavParams, private transfer: FileTransfer, private camera: Camera, public actionSheetCtrl: ActionSheetController, public globals: GlobalsProvider, public rest_call: RestProvider) {
    this.globals.showLoader("Loading...");
    if (this.navParams.get("post_Data")) {
      this.postData = this.navParams.get("post_Data");
      
      for (let i = 0; i < this.postData.post_media.length; i++) {
        const element = this.postData.post_media[i];
        this.old_photos.push(element.img_name);
        this.old_photos_urls.push(element.img_url);
      }
      this.postData.post_media = [];
    }
    this.rest_call.getCategories().then((result) => {
      if (result['status'] == "success") {
        this.categories = result['data'];
      } else {
        this.globals.presentToast("No Categories found....!!");
        this.navCtrl.pop();
      }
      this.globals.loading.dismiss();
    }, (err) => {

    });
     this.rest_call.get_friends(localStorage.getItem("user_id")).then((result) => {
      if (result['status'] == "success") {
        this.categories = result['data'];
      } else {
        this.globals.presentToast("No Categories found....!!");
        this.navCtrl.pop();
      }
      this.globals.loading.dismiss();
    }, (err) => {

    });

  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad NewPostPage');
  }
  gotoPreviousStat() {
    this.navCtrl.pop();
  }

  takePhoto() {
    let actionSheet = this.actionSheetCtrl.create({
      title: 'Choose Photos',
      buttons: [
        {
          text: 'Camera',
          role: 'destructive',
          handler: () => {

            this.camera.getPicture({
              sourceType: this.camera.PictureSourceType.CAMERA,
              destinationType: this.camera.DestinationType.DATA_URL
            }).then((imageData) => {
              this.selectedimage = 'data:image/jpeg;base64,' + imageData;
              let block = this.selectedimage.split(";");
              let contentType = block[0].split(":")[1];// In this case "image/gif"
              let realData = block[1].split(",")[1];//
              this.photo = this.b64toBlob(realData, contentType);
              this.postData.post_media.push(this.selectedimage);
              this.photos.push(this.selectedimage);
            }, (err) => {
              this.globals.presentToast(JSON.stringify(err));
            });
          }
        },
        {
          text: 'Gallery',
          handler: () => {
            this.camera.getPicture({
              sourceType: this.camera.PictureSourceType.SAVEDPHOTOALBUM,
              destinationType: this.camera.DestinationType.DATA_URL,
              encodingType: this.camera.EncodingType.JPEG,
              mediaType: this.camera.MediaType.PICTURE,
            }).then((imageData) => {
              this.selectedimage = 'data:image/jpeg;base64,' + imageData;
              let block = this.selectedimage.split(";");
              let contentType = block[0].split(":")[1];// In this case "image/gif"
              let realData = block[1].split(",")[1];//
              this.photo = this.b64toBlob(realData, contentType);
              this.postData.post_media.push(this.selectedimage);
              this.photos.push(this.selectedimage);
            }, (err) => {
              this.globals.presentToast(JSON.stringify(err));
            });
          }
        },
        {
          text: 'Cancel',
          role: 'cancel',
          handler: () => {
            console.log('Cancel clicked');
          }
        }
      ]
    });
    actionSheet.present();
  }
  createPost() {
    this.globals.showLoader('Creating post...');
    if (this.postData.post_id == '') {
      this.postData.insert = '1';
    } else if (this.postData.post_id != '') {
      this.postData.edit = '1';
    }
    this.rest_call.post_update(this.postData,this.old_photos).then((result) => {
      this.response = result;
      this.globals.presentToast(this.response.message);
      if (this.response.status == 'success') {
        this.app.getRootNav().push(TabsPage);
      }
    }, (err) => {
      console.log(err);
      alert(JSON.stringify(err));
    });
    this.globals.loading.dismiss();
  }
  uploadPhoto(post_id) {
    const fileTransfer: FileTransferObject = this.transfer.create();

    for (var i = 0; i < this.photos.length; i++) {
      var filename = 'post_media';
      let options: FileUploadOptions = {
        fileKey: filename,
        fileName: filename + ".jpeg",
        chunkedMode: false,
        mimeType: "image/jpeg",
        headers: {},
        params: { "post_user_id": localStorage.getItem("user_id"), "post_id": post_id, }
      }
      fileTransfer.upload(this.photos[i], this.rest_call.apiUrl + '/post_update.php', options)
        .then((data) => {
          alert(JSON.stringify(data) + " Uploaded Successfully");
        }, (err) => {
          console.log(err);
          alert(err);
        });
    }
  }

  b64toBlob(b64Data: any, contentType: any) {
    contentType = contentType || '';
    let sliceSize = 512 * 10;
    var byteCharacters = atob(b64Data);
    var byteArrays = [];
    for (var offset = 0; offset < byteCharacters.length; offset += sliceSize) {
      var slice = byteCharacters.slice(offset, offset + sliceSize);
      var byteNumbers = new Array(slice.length);
      for (var i = 0; i < slice.length; i++) {
        byteNumbers[i] = slice.charCodeAt(i);
      }
      var byteArray = new Uint8Array(byteNumbers);
      byteArrays.push(byteArray);
    }
    var blob = new Blob(byteArrays, { type: contentType });
    //blob.style.height = "300px";
    //blob.style.width = "300px";
    return blob;
  }

  selected_category(category_id) {
    let is_exists = this.postData.post_category.indexOf(category_id);
    if (is_exists > -1) {
      this.postData.post_category.splice(is_exists, 1);
    } else {
      this.postData.post_category.push(category_id);
    }
  }
  deletePhoto(rm_index) {
    this.photos.splice(rm_index, 1);
  }
  deleteOldPhoto(rm_index) {
    this.old_photos.splice(rm_index, 1);
    this.old_photos_urls.splice(rm_index, 1);
  }
  
}
