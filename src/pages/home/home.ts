import { Component } from '@angular/core';
import { NavController , App , LoadingController, ToastController } from 'ionic-angular';
import { LoginPage } from '../login/login';
import { AuthServiceProvider } from "../../providers/auth-service/auth-service";
import { OpenPostPage } from '../open-post/open-post';
import { GroupsPage } from '../groups/groups';
import { RestProvider } from '../../providers/rest/rest';        
import { global } from '@angular/core/src/util';
import { GlobalsProvider } from '../../providers/globals/globals';
@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage {
  loading: any;
  isLoggedIn: boolean = false;
  public posts:any= [];
  // You can get this data from your API. This is a dumb data for being an example.
  public stories = [
    {
      id: 1,
      img: 'https://source.unsplash.com/120x120',
      user_name: 'candelibas'
    }
  ];

  public categories : any;
  public page:any=1;
  public maximumPages:any = 1;
  reActiveInfinite: any;
  public post_category : any = [];
  
  constructor(public app: App, public rest_call: RestProvider, public navCtrl: NavController, public authService: AuthServiceProvider, public loadingCtrl: LoadingController, private toastCtrl: ToastController,private globals: GlobalsProvider) {
    
    if(localStorage.getItem("user_id")) {
      this.isLoggedIn = true;
    
    }else{
      this.app.getRootNav().push(LoginPage);
    }
  }

  ionViewDidLoad() {
    this.posts = [];
    this.page = 1;
    this.globals.showLoader("Fetching Data...");
    this.rest_call.getCategories().then((result) => {
      this.categories = result['data'];
      this.load_posts();      
    }, (err) => {
      alert(JSON.stringify(err));
    });
  }      
  load_posts(infiniteScroll?){
    this.rest_call.getPosts(this.page,this.post_category,'').then((result) => {
      this.posts = this.posts.concat(result['data']);
      this.maximumPages = result['maximumPages'];
      this.globals.loading.dismiss();
      if (infiniteScroll) {
        infiniteScroll.complete();
      }
    }, (err) => {
      console.log(err);
    });
  }      
  loadMore(infiniteScroll) {
    if (this.page === this.maximumPages) {
      infiniteScroll.enable(false);
    }else{
      this.page++;
      this.reActiveInfinite = infiniteScroll;
      this.load_posts(infiniteScroll);
    }
  }
  doRefresh(refresher) {
    this.ionViewDidLoad();
    // this.reActiveInfinite.enable(true);
    setTimeout(() => {
      refresher.complete();
    }, 2000);
  }
  selected_category(category_id) {
    let is_exists = this.post_category.indexOf(category_id);
    if (is_exists > -1) {
      this.post_category.splice(is_exists, 1);
    } else {
      this.post_category.push(category_id);
    }
    this.ionViewDidLoad();
    if(this.reActiveInfinite){
      this.reActiveInfinite.enable(true);
    }
    
  }
  logout() {
    this.showLoader();
    this.authService.logout().then((result) => {
      this.loading.dismiss();
      let nav = this.app.getRootNav();
      nav.setRoot(LoginPage);
    }, (err) => {
      this.loading.dismiss();
      console.log(err);
      this.presentToast(err);
    });
  }

  showLoader(){
    this.loading = this.loadingCtrl.create({
        content: 'Authenticating...'
    });
    this.loading.present();
  }


  presentToast(msg) {
    let toast = this.toastCtrl.create({
      message: msg,
      duration: 3000,
      position: 'bottom',
      dismissOnPageChange: true
    });

    toast.onDidDismiss(() => {
      console.log('Dismissed toast');
    });

    toast.present();
  }

  openPostPage(post_id){
    
    this.app.getRootNav().push(OpenPostPage,{
      post_id
    });
  }
  openMyGroups(){
    this.app.getRootNav().push(GroupsPage);
  }
}

