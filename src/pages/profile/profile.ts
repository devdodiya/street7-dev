import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, App } from 'ionic-angular';
import { InviteTeammatesPage } from '../invite-teammates/invite-teammates';
import { SettingsPage } from '../settings/settings';
import { EditProfilePage } from '../edit-profile/edit-profile';
import { InviteToStreet7Page } from '../invite-to-street7/invite-to-street7';
import { OpenPostPage } from '../open-post/open-post';
import { HomePage } from '../home/home';
import { RestProvider } from '../../providers/rest/rest';
import { GlobalsProvider } from '../../providers/globals/globals';

/**
 * Generated class for the ProfilePage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@Component({
  selector: 'page-profile',
  templateUrl: 'profile.html',
})
export class ProfilePage {
  public posts = [];
  public messages = [
    {
      id: 1,
      profile_img: 'https://source.unsplash.com/160x150',
      sender: 'Anna Blum',
      last_message: 'This sounded nonsense to Alice, so she said nothing, but set off at once toward...',
      time: '1:08 PM',
      review_star:5
    },
    {
      id: 2,
      profile_img: 'https://source.unsplash.com/160x150',
      sender: 'Iread Nathan',
      last_message: 'Thus much I thought proper to tell you in relation to yourself, and to the trust I...',
      time: 'YESTERDAY',
      review_star:5
    },
    {
      id: 3,
      profile_img: 'https://source.unsplash.com/160x150',
      sender: 'Christopher Ogden',
      last_message: 'OK!',
      time: 'YESTERDAY',
      review_star:5
    }
  ];
  userData = {
    'user_name': '',
    'user_mobilenumber': '',
    'user_email': '',
    'user_password': '',
    'confirmpassword': '',
    'last_name': '',
    'first_name': '',
    'facebook_user_id': '',
    'user_profile_picture':''
  };
  profile_tab : any;
  public footerIsHidden: boolean = false;
  public page:any=1;
  public categories : any;
  public maximumPages:any = 1;
  reActiveInfinite: any;

  constructor(public app: App,public navCtrl: NavController, public navParams: NavParams, public rest_call:RestProvider, public globals:GlobalsProvider) {
    console.log(this.navParams);
    this.profile_tab = "new_post";
    this.get_user_details();
  }

  get_user_details(){
    this.globals.showLoader("Loading");
    this.rest_call.get_user_details( this.navParams.data ).then((result) => {
      if ( result['status'] == "success" ) {
       this.userData.first_name = result['data']['first_name'];
       this.userData.last_name = result['data']['last_name'];
       this.userData.user_profile_picture = result['data']['user_profile_picture'];
      }else{
        alert(result['message']);
      }  
    }, (err) => {
      alert(err);
    });
    this.globals.hideLoader();
  }

  ionViewDidLoad() {
    this.posts = [];
    this.load_posts();   
  }

  load_posts(infiniteScroll?){
    this.globals.showLoader("Loading Posts");
    this.rest_call.getPosts(this.page,'',this.navParams.data).then((result) => {
      this.posts = this.posts.concat(result['data']);
      this.maximumPages = result['maximumPages']
      this.globals.loading.dismiss();
      if (infiniteScroll) {
        infiniteScroll.complete();
      }
    }, (err) => {
      console.log(err);
    });
  }      
  loadMore(infiniteScroll) {
    if (this.page === this.maximumPages) {
      infiniteScroll.enable(false);
    }else{
      this.page++;
      this.reActiveInfinite = infiniteScroll;
      this.load_posts(infiniteScroll);
    }
  }

  segmentChanged(event){
    if(event._value == "new_post"){
      this.ionViewDidLoad();
    }
    if(event._value == "reviews"){
      this.footerIsHidden = true;
    }else{
      this.footerIsHidden = false;
    }
  }
  getProfilImageFromGallry(){
    
  }
  gotoInviteContactlist(){
    this.app.getRootNav().push(InviteTeammatesPage);
  }
  invite_to_street7(){
    this.app.getRootNav().push(InviteToStreet7Page);
  }
  
  gotoSettingspage(){
    this.app.getRootNav().push(SettingsPage);    
  }
  gotoEditprofilepage(){
    this.app.getRootNav().push(EditProfilePage);    
    
  }
  openPostPage(post_id){
    this.app.getRootNav().push(OpenPostPage,{
      post_id
    });
  }
}
