import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams ,App, ViewController} from 'ionic-angular';
import { NewPostPage } from '../new-post/new-post';
import { RestProvider } from '../../providers/rest/rest';
import { GlobalsProvider } from '../../providers/globals/globals';

/**
 * Generated class for the PostPopoverPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-post-popover',
  templateUrl: 'post-popover.html',
})
export class PostPopoverPage {
  post_Data :any;
  constructor(public navCtrl: NavController, public navParams: NavParams,public app:App,public viewCtrl: ViewController,public rest_call:RestProvider,public globals:GlobalsProvider) {
    this.post_Data = navParams.get("post_Data");
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad PostPopoverPage');
  }
  editPost(){
    this.viewCtrl.dismiss();
    this.app.getRootNav().push(NewPostPage,{
      post_Data : this.post_Data
    });
  }
  deletePost(){
    this.viewCtrl.dismiss();
    this.globals.showLoader("Deleting post...");
    this.rest_call.delete_post(this.post_Data.post_id).then((result) => {
      this.globals.loading.dismiss();
      if (result['status'] == "success") {
        this.globals.presentToast("Post Deleted Successfully!!");
      } else {
        this.globals.presentToast("Error Deleting Post!!");
      }
    }, (err) => {
      console.log(err);
    }); 
  }
}
