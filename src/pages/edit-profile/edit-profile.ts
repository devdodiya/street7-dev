import { Component } from '@angular/core';
import { IonicPage ,NavController, NavParams, ActionSheetController, App } from 'ionic-angular';
import { RestProvider } from "../../providers/rest/rest";
import { GlobalsProvider } from "../../providers/globals/globals";
import { Camera, CameraOptions } from '@ionic-native/camera';
/**
 * Generated class for the EditProfilePage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-edit-profile',
  templateUrl: 'edit-profile.html',
})
export class EditProfilePage {
	newPhoto: any;
	photo: any;
	user_profile_picture: any = '';
	userPic: any = "assets/imgs/frame.png";
	options: CameraOptions = {
	    quality: 100,
	    sourceType: 0,
	    destinationType: this.camera.DestinationType.FILE_URI,
	    encodingType: this.camera.EncodingType.JPEG,
	    mediaType: this.camera.MediaType.PICTURE
	}
	userData = {
		'user_name': '',
    	'user_mobilenumber': '',
    	'user_email': '',
    	'user_password': '',
    	'confirmpassword': '',
    	'last_name': '',
    	'first_name': '',
    	'facebook_user_id': '',
    	'user_profile_picture':''
  	};
	constructor(public navCtrl: NavController, public navParams: NavParams,public rest_call: RestProvider, public actionSheetCtrl: ActionSheetController,private camera: Camera,public globals: GlobalsProvider) {
		this.get_user_details();
	}
	get_user_details(){
		this.rest_call.get_user_details( localStorage.getItem("user_id") ).then((result) => {
			if ( result['status'] == "success" ) {
				this.userData.user_name = result['data']['user_name'];
				this.userData.user_email = result['data']['user_email'];
				this.userData.user_mobilenumber = result['data']['user_mobilenumber'];
				this.userData.first_name = result['data']['first_name'];
				this.userData.last_name = result['data']['last_name'];
				this.userPic = result['data']['user_profile_picture'];
			}else{
				alert(result['msg']);
			}	
		}, (err) => {
			alert(err);
		});
	}
  user_update(){
  	if (this.userData.user_name && this.userData.user_mobilenumber && this.userData.user_email ) {
        console.log(this.userData)
        this.rest_call.user_update(this.userData, 0, this.user_profile_picture, 0, 1).then((result) => {
        	console.log(result);
			//  this.globals.loading.dismiss();
			// this.globals.presentToast("User Created Successfully");
			// this.navCtrl.pop();
			// this.loading.dismiss();
			// this.response = result;
			// this.navCtrl.pop();
        }, (err) => {
          console.log(err);
          //  this.globals.loading.dismiss();
          alert(JSON.stringify(err));
          this.globals.presentToast(JSON.stringify(err));
        });
	} else {
      this.globals.presentToast("Please fill all details");
    }
  }
  ionViewDidLoad() {
    console.log('ionViewDidLoad EditProfilePage');
  }
  getProfilImageFromCameraGallry() {
    let actionSheet = this.actionSheetCtrl.create({
      title: 'Choose Photos',
      buttons: [
        {
          text: 'Camera',
          role: 'destructive',
          handler: () => {
            this.camera.getPicture({
              sourceType: this.camera.PictureSourceType.CAMERA,
              destinationType: this.camera.DestinationType.DATA_URL
            }).then((imageData) => {
              this.userPic = 'data:image/jpeg;base64,' + imageData;
              this.newPhoto = imageData;
              this.user_profile_picture = 'data:image/jpeg;base64,' + imageData;
              let block = this.user_profile_picture.split(";");
              let contentType = block[0].split(":")[1];// In this case "image/gif"
              let realData = block[1].split(",")[1];//
              this.photo = this.b64toBlob(realData, contentType);

            }, (err) => {
              this.globals.presentToast(JSON.stringify(err));
            });
          }
        },
        {
          text: 'Gallery',
          handler: () => {
            this.camera.getPicture({
              sourceType: this.camera.PictureSourceType.SAVEDPHOTOALBUM,
              destinationType: this.camera.DestinationType.DATA_URL
            }).then((imageData) => {
              this.newPhoto = imageData;
              this.userPic = 'data:image/jpeg;base64,' + imageData;
              this.user_profile_picture = 'data:image/jpeg;base64,' + imageData;
              let block = this.user_profile_picture.split(";");
              let contentType = block[0].split(":")[1];// In this case "image/gif"
              let realData = block[1].split(",")[1];//
              this.photo = this.b64toBlob(realData, contentType);
            }, (err) => {
              this.globals.presentToast(JSON.stringify(err));

            });
          }
        },
        {
          text: 'Cancel',
          role: 'cancel',
          handler: () => {
            console.log('Cancel clicked');
          }
        }
      ]
    });
    actionSheet.present();
  }
  b64toBlob(b64Data: any, contentType: any) {
    contentType = contentType || '';
    let sliceSize = 512 * 10;
    var byteCharacters = atob(b64Data);
    var byteArrays = [];
    for (var offset = 0; offset < byteCharacters.length; offset += sliceSize) {
      var slice = byteCharacters.slice(offset, offset + sliceSize);
      var byteNumbers = new Array(slice.length);
      for (var i = 0; i < slice.length; i++) {
        byteNumbers[i] = slice.charCodeAt(i);
      }
      var byteArray = new Uint8Array(byteNumbers);
      byteArrays.push(byteArray);
    }
    var blob = new Blob(byteArrays, { type: contentType });
    console.log(JSON.stringify(blob));
    //blob.style.height = "300px";
    //blob.style.width = "300px";
    return blob;
  }
}
