import { Component, OnInit } from '@angular/core';
import { NavController, Platform, App, Tab, DateTime } from 'ionic-angular';
import { HomePage } from '../home/home';
import { TabsPage } from '../tabs/tabs';
import { RegisterPage } from '../register/register';
import { Facebook, FacebookLoginResponse } from '@ionic-native/facebook';
import { Network } from '@ionic-native/network';
import { RestProvider } from "../../providers/rest/rest";
import { GlobalsProvider } from "../../providers/globals/globals";
import * as firebase from 'firebase/app';
import { Observable } from 'rxjs';
import { Instagram } from "ng2-cordova-oauth/core";
import { TwitterConnect } from '@ionic-native/twitter-connect';
import { Push, PushObject, PushOptions } from '@ionic-native/push';
import { AngularFireDatabase } from '@angular/fire/database';
import { AngularFirestore } from "angularfire2/firestore";
import { User } from "../../app/app.models";
import { ChatService } from "../../app/app.services";
import { Storage } from '@ionic/storage';
import { ChatsPage } from "../chat/chat";
import { appconfig } from "../../app/app.config";

@Component({
  selector: 'page-login',
  templateUrl: 'login.html'
})
export class LoginPage {
  user: Observable<firebase.User>;

  loading: any;
  loginData = { user_email: '', user_password: '', time: new Date().getTime()};
  response: any;
  // platform:any;
  isLoggedIn:boolean;
  regData: any;
  base64_user_profile_pic:any;
  constructor(public app:App,public navCtrl: NavController, public rest_call: RestProvider, public globals: GlobalsProvider, public fb: Facebook, private _net: Network, public platform: Platform, public db: AngularFirestore, private twitter: TwitterConnect, private push: Push,private storage: Storage, private chatservice: ChatService,
    ) {
    this.platform = platform;
    this.regData = {
      user_name : "",
      user_mobilenumber : "",
      user_email : "",
      user_password : "",
      confirmpassword : "",
      first_name: "",
      last_name:"",
      facebook_user_id:""
    };
    if (!this.isConnected()) {
      alert("No Internet Available!! Please activate internet for better user experience ;)");
    }
    if(localStorage.getItem("user_id")) {
      this.isLoggedIn = true;
      this.navCtrl.setRoot(TabsPage);
    }
  }


  ionViewDidLoad() {
    // this.initPushNotification()
    /* this.storage.get("chatuser").then(chatuser => {
      if (chatuser && chatuser.email !== "") {
        this.navCtrl.push(ChatsPage);
      }
    }); */
  }
  isConnected(): boolean {
    let conntype = this._net.type;
    return conntype && conntype !== 'unknown' && conntype !== 'none';
  }

  initPushNotification() {
    const options: PushOptions = {
      android: {
        senderID: "454166531047"
      },
      ios: {
        alert: 'true',
        badge: true,
        sound: 'false'
      },
      windows: {},
      browser: {
        pushServiceURL: 'http://push.api.phonegap.com/v1/push'
      }
    };



    const pushObject: PushObject = this.push.init(options);
    pushObject.on('notification').subscribe((notification: any) => this.openhomepage());


    pushObject.on('registration').subscribe((registration: any) => this.savedevicetokentoserver(registration.registrationId));



    pushObject.on('error').subscribe(error => alert(error));




  }

  savedevicetokentoserver(token) {
    console.log(token);

  }

  openhomepage() {
    alert("login.ts debug 86");
  }
  doLogin() {
    // this.navCtrl.setRoot(HomePage);
    if (this.loginData.user_email && this.loginData.user_password) {


      this.globals.showLoader('Authenticating...');
      // this.rest_call.validateEmail(this.loginData.user_email);
      this.rest_call.login(this.loginData).then((result) => {
        this.response = result;
        this.globals.loading.dismiss();
        this.globals.presentToast(this.response.message);
        if (this.response.status == 'success') {
          localStorage.setItem('user_id', this.response.data.user_id);
          localStorage.setItem('user_name', this.response.data.user_name);
          localStorage.setItem('user_email', this.response.data.user_email);
          localStorage.setItem('user_status', this.response.data.user_status);

          this.db
          .collection<User>(appconfig.users_endpoint, ref => {
            return ref.where("email", "==", this.loginData.user_email);
          })
          .valueChanges()
          .subscribe(users => {
            if (users.length === 0) {
              //Register User

              //Add the timestamp
              this.loginData.time = new Date().getTime();

              this.chatservice
                .addUser(this.loginData)
                .then(res => {
                  //Registration successful
                  
                  this.storage.set("chatuser", this.loginData);
                  this.navCtrl.setRoot(TabsPage);
                })
                .catch(err => {
                  alert(err);
                });
            } else {
              //User already exists, move to chats page
              this.storage.get("chatuser");
            }
          
        });
        this.navCtrl.setRoot(TabsPage);
      }
    }, (err) => {
        this.globals.loading.dismiss();
        this.globals.presentToast("Wrong Email Or Password");
      });
    }else {
      this.globals.presentToast("Please enter email and password both");
    }
  }

  register() {
    this.navCtrl.push(RegisterPage);
  }



  do_facebook_login() {
    // Login with permissions
    this.fb.login(['email'])
      .then((res: FacebookLoginResponse) => {

        // The connection was successful
        if (res.status == "connected") {

          // Get user ID and Token
          var fb_id = res.authResponse.userID;
          var fb_token = res.authResponse.accessToken;

          this.fb.api("/me?fields=id,name,gender,birthday,email,first_name,last_name,picture.width(800).height(800)", []).then((user) => {
            
            /* var gender = user.gender;
            var birthday = user.birthday;
            var name = user.name;
            var email = user.email;
            var first_name = user.first_name;
            var last_name = user.last_name;
            var fb_user_id = user.id; */
            this.regData.user_name = user.email;
            this.regData.user_mobilenumber = "";
            this.regData.user_email = user.email;
            this.regData.user_password = user.id;
            this.regData.confirmpassword = user.id;
            this.regData.first_name = user.first_name;
            this.regData.last_name = user.last_name;
            this.regData.facebook_user_id = user.id;

            let imageUrl = user.picture.data.url;
            this.globals.getBase64ImageFromURL(imageUrl).subscribe(base64data => {
              this.base64_user_profile_pic = 'data:image/jpg;base64,' + base64data;
            });
            this.rest_call.user_update(this.regData,1,this.base64_user_profile_pic,'','').then((result) => {
              this.response = result;
              if(this.response['status'] == "success"){
                this.loginData.user_email = this.response['user_email'];
                this.loginData.user_password = this.response['user_password'];
                this.doLogin();
              }
              
            }, (err) => {
              this.globals.presentToast("Insert operation failed brooo");
            });
            
            // => Open user session and redirect to the next page

          });

        }
        // An error occurred while loging-in
        else {

          this.globals.presentToast('an error occured');

        }

      })
      .catch((e) => {
        this.globals.presentToast(e);
      });
  }



    /* do_instagram_login(){
     this.oauth.logInVia(this.provider).then((success) => {
                  alert(JSON.stringify(success));
              }, (error) => {
                  console.log(JSON.stringify(error));
              });
    } */

  do_twitter_login() {
    var provider = new firebase.auth.TwitterAuthProvider();
    this.twitter.login()
      .then(res => {
        
      }, err => {
        this.globals.presentToast("Twitter Not Installed");
      })
  }
}             