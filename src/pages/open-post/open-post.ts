import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, App, PopoverController } from 'ionic-angular';
import { SocialSharing } from '@ionic-native/social-sharing';
import { GlobalsProvider } from "../../providers/globals/globals";
import { RestProvider } from '../../providers/rest/rest';
import { ViewChild } from '@angular/core';
import { Slides } from 'ionic-angular';
import { NewPostPage } from '../new-post/new-post';
import { PostPopoverPage } from '../post-popover/post-popover';
/**
 * Generated class for the OpenPostPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */


@Component({
  selector: 'page-open-post',
  templateUrl: 'open-post.html',
})
export class OpenPostPage {
  @ViewChild(Slides) postslides: Slides;

  section: string = 'two';
  like: string = "md-heart-outline";
  somethings: any = new Array(20);
  post_id: any;
  user_comment: any;
  public post_slides = new Array();

  public post_comments = [];
  public post_Data: any;

  public post_title: string = "";
  public post_description: string = "";
  public post_by_user: string = "";
  public post_by_user_avatar: string = "";
  public post_likes: string = "";
  public post_shares: string = "";
  public post_views: string = "";
  public post_time: string = "";
  public show_edit_button: boolean = false;
  public page:any=1;
  public maximumPages:any = 1;

  constructor(public navCtrl: NavController, public navParams: NavParams, private socialSharing: SocialSharing, public globals: GlobalsProvider, public rest_call: RestProvider, public app: App, public popoverCtrl: PopoverController) {

    this.post_id = navParams.get('post_id');

    this.get_post_details();
    this.get_post_comments();

  }

  get_post_details() {
    this.globals.showLoader("");
    this.rest_call.add_post_view(this.post_id, localStorage.getItem("user_id")).then((result) => {

    }, (err) => {

    });
    this.rest_call.getPostsDetails(this.post_id).then((result) => {
      if (result['status'] == "success") {
        this.post_Data = result['data'];

        if (this.post_Data.post_user_id == localStorage.getItem("user_id")) {
          this.show_edit_button = true;
        }
        this.post_title = this.post_Data.post_title;
        this.post_by_user = this.post_Data.post_by_user;
        this.post_by_user_avatar = this.post_Data.post_by_user_avatar;
        this.post_description = this.post_Data.post_description;
        this.post_time = this.post_Data.post_time;
        this.post_likes = this.post_Data.post_likes;
        this.post_views = this.post_Data.post_views;
        this.post_shares = this.post_Data.post_shares;

        this.post_slides = this.post_Data.post_media;
        this.postslides.parallax = true;
        this.postslides.autoplay = 2000;
        this.postslides.loop = true;


        if (this.post_Data.check_if_post_liked) {
          this.like = "md-heart";
        }
      } else {
        this.globals.presentToast("Post Not Found!!");
        this.navCtrl.pop();
      }
    }, (err) => {

    });
  }
  get_post_comments(){
    this.rest_call.get_post_comments(this.post_id, this.page).then((result) => {
      this.post_comments = this.post_comments.concat(result['data']);
    }, (err) => {
      console.log(err);
    });
    this.globals.hideLoader();
  }
  post_like() {
    this.rest_call.post_like(this.post_id).then((result) => {
      if (result['status'] == "success" && result['check_if_post_liked']) {
        this.like = "md-heart"
      } else {
        this.like = "md-heart-outline"
      }
    }, (err) => {
      console.log(err);
    });
  }
  open_post_option(myEvent) {
    let popover = this.popoverCtrl.create(PostPopoverPage, { post_Data: this.post_Data });
    popover.present({
      ev: myEvent
    });
  }
  share() {
    this.socialSharing.share('street7 is a amazing app', 'street7', "gdgdgdgd", 'www.street7.com').then((res) => {
      console.log(res)
    }).catch((err) => {
      this.globals.presentToast(err);
    });
  }
  add_comment() {
    this.globals.showLoader("");
    if (this.user_comment != "") {
      this.rest_call.add_comment('insert',this.user_comment, this.post_id, localStorage.getItem("user_id")).then((result) => {
        
      }, (err) => {
        
        this.globals.presentToast("Wrong Email Or Password");
      });
    }
    this.globals.loading.dismiss();
  }
}      
