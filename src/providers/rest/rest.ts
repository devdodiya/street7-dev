import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import 'rxjs/add/operator/map';
import * as firebase from 'firebase/app';
import { Observable } from 'rxjs';
import { GlobalsProvider } from '../globals/globals';
/*
  Generated class for the RestProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/

@Injectable()
export class RestProvider {
  apiUrl = 'http://demo.aarvitechnolabs.com/street7-api/api';
  user: Observable<firebase.User>;
  constructor(public http: HttpClient,public globals:GlobalsProvider) {
    console.log('Hello RestProvider Provider');
  }

  /* user manage apis */
  login(credentials) {
    return new Promise((resolve, reject) => {
      let headers = new HttpHeaders();
      headers.append('Content-Type', 'application/json');
      var frm_data = new FormData();
      frm_data.append("user_email", credentials.user_email);
      frm_data.append("user_password", credentials.user_password);
      this.http.post(this.apiUrl + '/user_login.php', frm_data, { headers: headers })
        .subscribe(res => {
          resolve(res);
        }, (err) => {
          reject(err);
        });
    });
  }
  user_update(regData, signup, photo, del, update) {
    return new Promise((resolve, reject) => {
      let headers = new HttpHeaders();
      headers.append('Content-Type', 'application/json');
      var frm_data = new FormData();
      
      frm_data.append('user_name', regData.user_name);
      frm_data.append('user_mobilenumber', regData.user_mobilenumber);
      frm_data.append('user_email', regData.user_email);
      frm_data.append('user_password', regData.user_password);
      frm_data.append('confirmpassword', regData.confirmpassword);
      frm_data.append('last_name', regData.last_name);
      frm_data.append('first_name', regData.first_name);
      frm_data.append('facebook_user_id', regData.facebook_user_id);
      frm_data.append('insert', signup);
      frm_data.append('edit', update);
      frm_data.append('delete', del);
      frm_data.append('base64_user_profile_pic', photo);
      
      if(update == '1'){
        frm_data.append('user_id', localStorage.getItem("user_id"));
      }

      this.http.post(this.apiUrl + '/user_update.php', frm_data, { headers: headers })
        .subscribe(res => {
          resolve(res);
        }, (err) => {
          reject(err);
        });
    });
  }
  post_update(postData,old_photos) {
    return new Promise((resolve, reject) => {
      let headers = new HttpHeaders();
      headers.append('Content-Type', 'application/json');
      var frm_data = new FormData();
      frm_data.append('post_id', postData.post_id);
      frm_data.append('post_title', postData.post_title);
      frm_data.append('post_description', postData.post_description);
      frm_data.append('post_user_id', postData.post_user_id);
      
      for (let index = 0; index < postData.post_category.length; index++) {
        const element = postData.post_category[index];
        frm_data.append('post_category['+index+']', element);
      }
      frm_data.append('insert', postData.insert);
      frm_data.append('edit', postData.edit);
      frm_data.append('delete', postData.delete);
      for (let index = 0; index < postData.post_media.length; index++) {
        const element = postData.post_media[index];
        frm_data.append('post_media['+index+']', element);  
      }
      for (let index = 0; index < old_photos.length; index++) {
        const element = old_photos[index];
        frm_data.append('old_photos['+index+']', element);  
      }
      
      this.http.post(this.apiUrl + '/post_update.php', frm_data, { headers: headers })
        .subscribe(res => {
          resolve(res);
        }, (err) => {
          reject(err);
        });
    });
  }
  getPosts(page,post_category,user_id) {
    return new Promise((resolve, reject) => {

      var frm_data = new FormData();
      if (user_id != '') {
        frm_data.append('post_user_id', user_id);  
      }
      for (let index = 0; index < post_category.length; index++) {
        const element = post_category[index];
        frm_data.append('post_category['+index+']', element);  
      }
      frm_data.append('page', page);
      frm_data.append('per_page', this.globals.per_page);
      this.http.post(this.apiUrl + '/get_posts.php', frm_data)
        .subscribe(res => {
          resolve(res);
        }, (err) => {
          reject(err);
        });
    });
  }
  getPostsDetails(post_id) {
    return new Promise((resolve, reject) => {

      var frm_data = new FormData();

      frm_data.append('post_id', post_id);   
      frm_data.append('requested_user_id', localStorage.getItem("user_id"));   
      
      this.http.post(this.apiUrl + '/get_post_details.php', frm_data)
        .subscribe(res => {
          resolve(res);
        }, (err) => {
          reject(err);
        });
    });
  }
  post_like(post_id){
    return new Promise((resolve, reject) => {
      var frm_data = new FormData();
      frm_data.append('post_id', post_id);
      frm_data.append('requested_user_id', localStorage.getItem("user_id"));
      this.http.post(this.apiUrl + '/post_like.php', frm_data)
        .subscribe(res => {
          resolve(res);
        }, (err) => {
          reject(err);
        });
    });
  }
  logout() {
    return new Promise((resolve, reject) => {
      let headers = new HttpHeaders();
      headers.append('X-Auth-Token', localStorage.getItem('token'));

      this.http.post(this.apiUrl + 'logout', {}, { headers: headers })
        .subscribe(res => {
          localStorage.clear();
        }, (err) => {
          reject(err);
        });
    });
  }


  validateEmail(email) {
    var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    return re.test(String(email).toLowerCase());

  }

  getCategories(){
    return new Promise((resolve, reject) => {
      var frm_data = new FormData();
      frm_data.append('post_user_id', localStorage.getItem("user_id"));
      this.http.post(this.apiUrl + '/get_category.php', frm_data)
        .subscribe(res => {
          resolve(res);
        }, (err) => {
          reject(err);
        });
    });
  }
  add_post_view(post_id,user_id){
    return new Promise((resolve, reject) => {
      var frm_data = new FormData();
      frm_data.append('post_id', post_id);
      frm_data.append('requested_user_id', user_id);
      this.http.post(this.apiUrl + '/add_post_view.php', frm_data)
        .subscribe(res => {
          resolve(res);
        }, (err) => {
          reject(err);
        });
    });
  }
  delete_post(post_id){
    return new Promise((resolve, reject) => {
      var frm_data = new FormData();
      frm_data.append('post_id', post_id);
      frm_data.append('requested_user_id', localStorage.getItem("user_id"));
      this.http.post(this.apiUrl + '/delete_post.php', frm_data)
        .subscribe(res => {
          resolve(res);
        }, (err) => {
          reject(err);
        });
    });
  }
  get_user_details(user_id){
    return new Promise((resolve, reject) => {
      var frm_data = new FormData();
      frm_data.append('user_id', user_id);
      this.http.post(this.apiUrl + '/get_user_details.php', frm_data)
        .subscribe(res => {
          resolve(res);
        }, (err) => {
          reject(err);
        });
    });
  }
 get_friends(user_id){
    return new Promise((resolve, reject) => {
      var frm_data = new FormData();
      frm_data.append('user_id', user_id);
      this.http.post(this.apiUrl + '/get_friends.php', frm_data)
        .subscribe(res => {
          resolve(res);
        }, (err) => {
          reject(err);
        });
    });
  }
  add_comment(comment_id,comment,post_id,user_id) {
    return new Promise((resolve, reject) => {
      let headers = new HttpHeaders();
      headers.append('Content-Type', 'application/json');
      var frm_data = new FormData();
      
      frm_data.append("post_id", post_id);
      frm_data.append("user_id", user_id);
      frm_data.append("content", comment);
      frm_data.append("comment_id", comment_id);

      this.http.post(this.apiUrl + '/post_comment.php', frm_data, { headers: headers })
        .subscribe(res => {
          resolve(res);
        }, (err) => {
          reject(err);
        });
    });
  }
  get_post_comments(post_id,page){
    return new Promise((resolve, reject) => {
      var frm_data = new FormData();
      frm_data.append('post_id', post_id);
      frm_data.append('page', page);
      frm_data.append('per_page', this.globals.per_page);
      this.http.post(this.apiUrl + '/get_post_comment.php', frm_data)
        .subscribe(res => {
          resolve(res);
        }, (err) => {
          reject(err);
        });
    });
  }  
}
